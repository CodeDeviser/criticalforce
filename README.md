# criticalforce



## Installation

1.Clone the project

2.Turn on Docker (Make sure you have the latest version)

3.On Docker go to settings/FileSharing and add your project folder

4.On your project folder run:

For MacOS and Linux just run the script

For Windows you have to convert those scripts first with unix2dos for example, so windows can read properly bash scripts.

```
sh start_project.sh
```

5.Give at least 2 to 5 minutes so docker can setup all containers and composer can install all dependecies. check in docker on php service if is installing dependecies.

6.After that you can access to services on:

Localization
```
http://localhost:8000/api/languages
```

Products
```
http://localhost:8001/api/products
```

There is a file in root folder that have all the endpoints available to test with postman tool.



The idei behind this structure is to have a aditional table in localization service for each new service, for example:

Products service -> new table products_translations
Other service.   -> new table other_translations

this way we have to write the double off tables and controllers and models but, we can have everything separated, easy to query and easy to maintain and understand.

things to improve:

- Make a 2 factor authentication.
- Have a gateway orchestrating the microservices with token bearer for security and permissions
- Automate the addition of a new table for translate in localization service 
- Write a pipeline for testing and deployment
