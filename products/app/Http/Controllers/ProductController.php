<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return all products from Database
     * 
     *  @return \Illuminate\Http\Response
     */
    public function all() {
        $products = Product::all();
        return response()->json($products);
    }

    /**
     * Return all products by language id from Database
     * 
     *  @return \Illuminate\Http\Response
     */
    public function allByLanguage($language_id) {
        $products = Product::where('language_id', $language_id)->get();
        return response()->json($products);
    }

    /**
     * Return product by id from Database
     *  @param mixed $id
     *  @return \Illuminate\Http\Response
     */
    public function show($id) {
        $product = Product::find($id);
        return response()->json($product);
    }

    /**
     * Store a newly created product in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'price' => 'required',
        ]);
            
        $product = Product::create([
            'price' => $validatedData['price'],
        ]);
        
        $response = [
            'success' => true,
            'data'    => $product,
            'message' => 'Product created successfully!',
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified product in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $this->validate($request, [
            'price' => 'required',
        ]);

       $product = Product::find($id);

       $product->price = $request->price;
       $product->updated_at = Carbon::now()->format('Y-m-d H:i:s');

       $product->save();

       $response = [
            'success' => true,
            'data'    => $product,
            'message' => 'Product updated successfully!',
        ];

        return response()->json($response, 200);

    }
   
    /**
     * Remove the specified product from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $product = Product::find($id);
        $product->delete();

        $response = [
            'success' => true,
            'data'    => $product,
            'message' => 'Product successfully deleted!',
        ];

        return response()->json($response, 200);
    }
}
