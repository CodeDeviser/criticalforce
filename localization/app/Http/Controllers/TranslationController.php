<?php

namespace App\Http\Controllers;

use App\Models\Translation;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\HandlerStack;

class TranslationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return all translations from Database
     * 
     *  @return \Illuminate\Http\Response
     */
    public function all() 
    {
        $services = Translation::all();
        $translations = [];
        foreach($services as $service){
            $controller = "\App\Http\Controllers\\" . $service->controller;
            $translations[$service->name] = (new $controller)->all();
        }

        return response()->json($translations);
    }

    /**
     * Return all translations by language from Database
     * 
     *  @return \Illuminate\Http\Response
     */
    public function allByLanguage($language_id)
    {
        $services = Translation::all();
        $translations = [];
        foreach($services as $service){
            $controller = "\App\Http\Controllers\\" . $service->controller;
            $translations[$service->name] = (new $controller)->allByLanguage($language_id);
        }

        return response()->json($translations);
    }

    /**
     * Return translation by id from Database
     *  @param mixed $id
     *  @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
       //TODO
    }

    /**
     * Store a newly created translation in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //TODO
    }

    /**
     * Update the specified translation in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //TODO
    }
   
    /**
     * Remove the specified translation from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
       //TODO
    }
}
