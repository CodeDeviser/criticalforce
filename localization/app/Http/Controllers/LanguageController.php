<?php

namespace App\Http\Controllers;

use App\Models\Language;
use Illuminate\Http\Request;
use Carbon\Carbon;

class LanguageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return all Languages from Database
     * 
     *  @return \Illuminate\Http\Response
     */
    public function all()
    {
        $languages = Language::all();
        return response()->json($languages);
    }

    /**
     * Return Language by id from Database
     *  @param mixed $id
     *  @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        $language = Language::find($id);
        return response()->json($language);
    }

    /**
     * Store a newly created language in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name' => 'required|string|max:255',
            'code' => 'required|string|max:2',
        ]);
            
        $language = Language::create([
            'name' => $validatedData['name'],
            'code' => $validatedData['code'],
        ]);
        
        $response = [
            'success' => true,
            'data'    => $language,
            'message' => 'Language created successfully!',
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified language in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $this->validate($request, [
            'name' => 'required|string|max:255',
            'code' => 'required|string|max:2',
        ]);

       $language = Language::find($id);

       $language->name = $request->name;
       $language->code = $request->code;
       $language->updated_at = Carbon::now()->format('Y-m-d H:i:s');

       $language->save();

       $response = [
            'success' => true,
            'data'    => $language,
            'message' => 'Language updated successfully!',
        ];

        return response()->json($response, 200);

    }
   
    /**
     * Remove the specified language from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $language = Language::find($id);
        $language->delete();

        $response = [
            'success' => true,
            'data'    => $language,
            'message' => 'Language successfully deleted!',
        ];

        return response()->json($response, 200);
    }
}
