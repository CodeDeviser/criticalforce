<?php

namespace App\Http\Controllers;

use App\Models\ProductTranslation;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ProductTranslationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return all product translations from Database
     * 
     *  @return \Illuminate\Http\Response
     */
    public function all() 
    {
        $translations = ProductTranslation::all();
        return response()->json($translations);
    }

    /**
     * Return all product translations from Database
     * 
     *  @return \Illuminate\Http\Response
     */
    public function allByLanguage($language_id) 
    {
        $translations = ProductTranslation::where('language_id', $language_id)->get();
        return response()->json($translations);
    }

    /**
     * Return product translation by id from Database
     *  @param mixed $id
     *  @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        $translation = ProductTranslation::find($id);
        return response()->json($translation);
    }

    /**
     * Store a newly created product translation in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'ms' => 'required',
            'language_id' => 'required',
            'product_id' => 'required',
            'title' => 'required',
            'description' => 'required'
        ]);
            
        $translation = ProductTranslation::create([
            'product_id' => $validatedData['product_id'],
            'language_id' => $validatedData['language_id'],
            'title' => $validatedData['title'],
            'description' => $validatedData['description']
        ]);
        
        $response = [
            'success' => true,
            'data'    => $translation,
            'message' => 'Product translation created successfully!',
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified product translation in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $this->validate($request, [
            'ms' => 'required',
            'language_id' => 'required',
            'product_id' => 'required',
            'title' => 'required',
            'description' => 'required'
        ]);

       $translation = ProductTranslation::find($id);

       $translation->language_id = $request->language_id;
       $translation->product_id = $request->product_id;
       $translation->title      = $request->title;
       $translation->description= $request->description;
       $translation->updated_at = Carbon::now()->format('Y-m-d H:i:s');

       $translation->save();

       $response = [
            'success' => true,
            'data'    => $translation,
            'message' => 'Product translation updated successfully!',
        ];

        return response()->json($response, 200);

    }
   
    /**
     * Remove the specified product translation from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $translation = ProductTranslation::find($id);
        $translation->delete();

        $response = [
            'success' => true,
            'data'    => $translation,
            'message' => 'Product translation successfully deleted!',
        ];

        return response()->json($response, 200);
    }
}
