<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/'], function() use ($router) {

	/*
     * Languages Routes
     */
    $router->get('languages', 'LanguageController@all');
    $router->get('languages/{id}', 'LanguageController@show');
    $router->post('languages', 'LanguageController@store');
    $router->put('languages/{id}', 'LanguageController@update');
    $router->delete('languages/{id}', 'LanguageController@delete');

    /*
     * Translations Controller 
     */
    $router->get('translations', 'TranslationController@all');
    $router->get('translationsbylanguage/{language_id}', 'TranslationController@allByLanguage');

    /*
     * Product Translations Routes
     */
    $router->get('producttranslations', 'ProductTranslationController@all');
    $router->get('producttranslationsbylanguage/{language_id}', 'ProductTranslationController@allByLanguage');
    $router->get('producttranslations/{id}', 'ProductTranslationController@show');
    $router->post('producttranslations', 'ProductTranslationController@store');
    $router->put('producttranslations/{id}', 'ProductTranslationController@update');
    $router->delete('producttranslations/{id}', 'ProductTranslationController@delete');
});
