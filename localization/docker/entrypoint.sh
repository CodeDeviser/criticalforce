#!/bin/bash

if [ ! -f "vendor/autoload.php" ]; then
	composer install --no-progress --no-interaction
fi

if [ ! -f ".env" ]; then
	echo "Creating env file for env $$APP_ENV"
	cp .env.example .env
else
	echo ".env file exists."
fi

#Wait until database is ready and composer install all dependencies 
echo "Installing dependencies..."
sleep 2m

composer require guzzlehttp/guzzle

php artisan migrate:fresh
php artisan db:seed
php artisan key:generate
php artisan cache:clear
php artisan config:clear
php artisan route:clear

php -S 0.0.0.0:8000 -t ./public

#php artisan serve --port=$PORT --host=0.0.0.0 --env=.env
exec docker-php-entrypoint "$@"
