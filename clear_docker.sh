#!/bin/bash
#stop containers
docker-compose down
#Clear image volume and cache
docker container rm $(docker container ls -a -q) -f
docker image rm $(docker image ls -a -q)
docker volume rm $(docker volume ls -q)
docker builder prune --all